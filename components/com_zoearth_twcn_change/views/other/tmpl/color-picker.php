<?php
defined('_JEXEC') or die;
//20141030 zoearth 這邊呼叫步驟
$step = (int)JRequest::getVar('step','');
$cartItems = Z2CartS::getCartItems();
if (count($cartItems) > 0 )
{
    switch ($step)
    {
        default:
        case '1':require_once dirname(__FILE__).'/cart_step1.php';break;
        case '2':require_once dirname(__FILE__).'/cart_step2.php';break;
        case '3':require_once dirname(__FILE__).'/cart_step3.php';break;
        case '4':require_once dirname(__FILE__).'/cart_step4.php';break;
    }
}
else
{
    require_once dirname(__FILE__).'/cart_null.php';
}
?>