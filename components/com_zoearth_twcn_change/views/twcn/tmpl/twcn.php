<?php
defined('_JEXEC') or die;
?>
<script language="Javascript">
jQuery(document).ready(function() {
    //顯示已輸入文字
    jQuery("#gotoInput").on('keydown change',function(){
        var gotoInput = jQuery("#gotoInput").val();
        jQuery("#wordCount").html(gotoInput.length);
    });
    //送出資料
    jQuery(".goChange").click(function(){
        var lang = jQuery(this).attr('lang');
        var gotoInput = jQuery("#gotoInput").val();
        
        if (gotoInput == '')
        {
            alert("<?php echo JText::_('COM_ZOEARTH_PLS_INPUT_WORD')?>");return false;
        }
        else if (gotoInput.length >= 65535)
        {
            alert("<?php echo JText::_('COM_ZOEARTH_TOO_MANY_WORD')?>");return false;
        }
        else if (!lang)
        {
            alert("<?php echo JText::_('COM_ZOEARTH_ERROR_SORRY')?>!!");return false;
        }
        else
        {
            jQuery(".goChange").attr('disabled','disabled');
            jQuery("#gotoInput").attr('disabled','disabled');
            jQuery.post(location.href,{ "task": "Trans","type":lang,'str':gotoInput },function(data){
                if (data.result != '1')
                {
                    alert("<?php echo JText::_('COM_ZOEARTH_ERROR_SORRY')?>!");
                }
                jQuery("#gotoInput").val(data.output);

                jQuery(".goChange").attr('disabled',false);
                jQuery("#gotoInput").attr('disabled', false);
                
            }, "json").error(function() { alert("<?php echo JText::_('COM_ZOEARTH_ERROR_SORRY')?>"); });
        }

    });
});
</script>
<style type="text/css">
#gotoInput {
    font-size: 24px;
    height: 500px;
    line-height: 32px;
    width: 100%;
}
</style>

<form>
    <fieldset>
    <legend><?php echo JText::_('COM_ZOEARTH_TW_TO_CN')?></legend>
    <?php echo Z2HelperRelateMenu::$data->description; ?>
    <div class="text-center">
        <input class="btn goChange" lang="twcn" type="button" value="<?php echo JText::_('COM_ZOEARTH_SAY_TW_TO_CN')?>">
        <input class="btn goChange" lang="cntw" type="button" value="<?php echo JText::_('COM_ZOEARTH_SAY_CN_TO_TW')?>">
    </div>
    <textarea id="gotoInput" rows="3" placeholder="<?php echo JText::_('COM_ZOEARTH_PLS_INPUT_WORD')?>" ></textarea>
    <span class="label label-success"><?php echo JText::_('COM_ZOEARTH_HAVE_INPUT')?>:<span id="wordCount">0</span><?php echo JText::_('COM_ZOEARTH_WORD')?></span>
    </fieldset>
</form>