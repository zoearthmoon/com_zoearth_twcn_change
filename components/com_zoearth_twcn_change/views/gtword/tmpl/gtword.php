<?php
defined('_JEXEC') or die;
?>
<style type="text/css">
#gotoInput {
    font-size: 24px;
    height: 500px;
    line-height: 32px;
    width: 100%;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery(".goGtword").unbind().click(function(){
        var input = jQuery("#gotoInput").val();
        input = input.replace(/<\/ /g,"</");

        for (var i=65;i<=90;i++)
        {
            var preg = "<"+String.fromCharCode(i);
            var preg2 = "<\/"+String.fromCharCode(i);
            preg  = new RegExp(preg,"g");
            preg2 = new RegExp(preg2,"g");

            var rep   = '<'+String.fromCharCode(i).toLowerCase();
            var rep2  = '</'+String.fromCharCode(i).toLowerCase();

            input = input.replace(preg,rep);
            input = input.replace(preg2,rep2);
        }

        input = input.replace(/<H([0-9]{1})/g,"<h$1");
        input = input.replace(/ = /g,"=");
        input = input.replace(/<\/ /g,"</");
        input = input.replace(/< \//g,"</");
        input = input.replace(/< /g,"<");
        input = input.replace(/\& nbsp;/g,"&nbsp;");
        input = input.replace(/ \/ /g,"\/");
        input = input.replace(/ \# /g,"#");
        input = input.replace(/\"\/ /g,"\"\/");
        jQuery("#gotoInput").val(input);
    });
});
</script>
<form>
    <fieldset>
    <legend><?php echo JText::_('COM_ZOEARTH_TWCN_GTWORD')?></legend>
    <?php echo Z2HelperRelateMenu::$data->description; ?>
    <div class="text-center">
        <input class="btn goGtword" lang="twcn" type="button" value="<?php echo JText::_('COM_ZOEARTH_TWCN_RUN')?>">
    </div>
    <textarea id="gotoInput" rows="3" placeholder="<?php echo JText::_('COM_ZOEARTH_PLS_INPUT_WORD')?>" ></textarea>
    </fieldset>
</form>