<?php
/*
@author zoearth
*/
defined('_JEXEC') or die('Restricted access');

define('CONTROLLER','Cart');
define('CONTROLLER_NAME','購物車');
define('CONTROLLER_BASE_URL',Juri::base().'index.php?option='.COM_NAME.'&view='.CONTROLLER);
class Z2CartControllerCart extends ZoeController
{
    function display($cachable = false, $urlparams = false)
    {
        $this->index();
    }
    
    function index()
    {
        //20140425 zoearth Joomla 必須先設定模板
        //20140424 zoearth 設定模板
        $view = $this->getDisplay(CONTROLLER.'/cart');
        $this->getOptions(); //20130729 zoearth 選單資料
        $this->setupParams(array()); //20140425 zoearth 搜尋欄位
    
        //20141023 zoearth 取得資料應該放在helper
        //$Cart_DB = $this->getModel('Cart');
        //$cartItems = $Cart_DB->loadCart();
        //$this->viewData['cartItems'] = $cartItems;
        //$this->viewData['cartTotal'] = $Cart_DB->nowT;
        
        //20141022 zoearth 取得loading模板
        $mainframe = JFactory::getApplication();
        $loadingHtml = '';
        if (JFile::exists(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.'com_z2_cart'.DS.'cart'.DS.'loading.php'))
        {
            $tplPath = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.'com_z2_cart'.DS.'cart';
            $layout = new JLayoutFile('loading',$tplPath);
            $loadingHtml = $layout->render();
        }
        $this->viewData['loadingHtml'] = $loadingHtml;

        $view->assignRef('data', $this->viewData);
        $option = array();
        $view->display();
    }
    
    //20141007 zoearth 修改購物車
    function edit()
    {
        //20140424 zoearth 設定模板
        $view = $this->getDisplay(CONTROLLER.'/cart');
        $this->getOptions(); //20130729 zoearth 選單資料
        $this->setupParams(array()); //20140425 zoearth 搜尋欄位
        
        //20141024 zoearth 編輯購物車
        $Cart_DB = $this->getModel('Cart');
        $Cart_DB->editCart();
        
//         $cartItems = $Cart_DB->loadCart();
//         $this->viewData['cartItems'] = $cartItems;
//         $this->viewData['cartTotal'] = $Cart_DB->nowT;
        
        $view->assignRef('data', $this->viewData);
        $option = array();
        $view->display();
    }
    
    //20141007 zoearth 加入購物車
    function add()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $Cart_DB = $this->getModel('Cart');
            $message = $Cart_DB->addCart();
            $output = array();
            $output['message'] = $message;
            
            //20141008 zoearth 設定 $message 的模板
            $mainframe = JFactory::getApplication();
            $tplPath = JPATH_SITE.DS.'components'.DS.'com_z2_cart'.DS.'views'.DS.'cart'.DS.'tmpl';
            if (JFile::exists(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.'com_z2_cart'.DS.'cart'.DS.'message.php'))
            {
                $tplPath = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.'com_z2_cart'.DS.'cart';
            }
            $layout = new JLayoutFile('message',$tplPath);
            $output['message'] = $layout->render($output);
            
            echo json_encode($output);
            exit();
        }
    }
    
    //20140424 zoearth 取得編輯介面會需要用到的選單
    function getOptions()
    {
    
    }
}