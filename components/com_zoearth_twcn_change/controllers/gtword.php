<?php
/*
@author zoearth
*/
defined('_JEXEC') or die('Restricted access');

define('CONTROLLER','gtword');
define('CONTROLLER_NAME','Google翻譯HTML整理');
define('CONTROLLER_BASE_URL',Juri::base().'index.php?option='.COM_NAME.'&view='.CONTROLLER);
class ZoearthTwcnChangeControllerGtword extends ZoeController
{
    function display($cachable = false, $urlparams = false)
    {
        $this->index();
    }
    
    function index()
    {
        //20140425 zoearth Joomla 必須先設定模板
        $view = $this->getDisplay(CONTROLLER.'/gtword');
        $view->display();
    }
}