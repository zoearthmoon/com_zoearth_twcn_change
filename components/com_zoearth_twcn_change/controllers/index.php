<?php
/*
@author zoearth
*/
defined('_JEXEC') or die('Restricted access');

class Z2CartControllerIndex extends JControllerLegacy
{
    function display($cachable = false, $urlparams = false)
    {
        $this->setRedirect('index.php?option=com_z2_cart&view=Order','', 'notice');
    }
}