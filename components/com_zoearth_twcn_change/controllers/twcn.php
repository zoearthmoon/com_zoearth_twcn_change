<?php
/*
@author zoearth
*/
defined('_JEXEC') or die('Restricted access');

define('CONTROLLER','Twcn');
define('CONTROLLER_NAME','繁簡互轉');
define('CONTROLLER_BASE_URL',Juri::base().'index.php?option='.COM_NAME.'&view='.CONTROLLER);
class ZoearthTwcnChangeControllerTwcn extends ZoeController
{
    function display($cachable = false, $urlparams = false)
    {
        $this->index();
    }
    
    function index()
    {
        //20140425 zoearth Joomla 必須先設定模板
        $view = $this->getDisplay(CONTROLLER.'/twcn');
        $view->display();
    }
    
    //20141113 zoearth 轉換
    function Trans()
    {
        if ($this->isPost())
        {
            $Twcn_DB = $this->getModel('Twcn');
            //20141207 zoearth 開始轉換
            $type = JRequest::getVar('type',NULL);
            $str  = JRequest::getVar('str',NULL);
            if (!in_array($type,array('twcn','cntw')))
            {
                echo json_encode(array('result'=>JText::_('COM_ZOE_TYPE_ERROR')));
                exit();
            }
            if (trim($str) == '')
            {
                echo json_encode(array('result'=>JText::_('COM_ZOEARTH_PLS_INPUT_WORD')));
                exit();
            }
            $newStr = $Twcn_DB->getTrance($type,$str);
            echo json_encode(array('result'=>1,'output'=>$newStr));
            exit();
        }
        else
        {
            echo json_encode(array('result'=>JText::_('COM_ZOE_POST_ERROR')));
            exit();
        }
    }
}